#!/bin/sh

# Copyright (C) 2021 Alberto Parra Espejo <aparra1998@protonmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
   
todoFilePath="/home/$(whoami)/.TODO";

help() {
cat << EOF
todo <operations> [...]
operations:
	todo {-a --add} [line(s)]
	todo {-d --delete} [line number(s)]
	todo {-h --help}
	todo {-s --show}
EOF
}

show() {
if [ $(cat $todoFilePath | wc -l) -gt 0 ]; then
	nl $todoFilePath;
fi
}

add() {
if [ -n "$1" ]; then
	for param in "$@"
	do
		echo -e "$param" >> $todoFilePath;
	done
else
	read -p "Type the pending task:" todoLine;
	echo -e $todoLine >> $todoFilePath;
fi
}

delete(){
if [ -n "$1" ]; then
	sedCommand="";
	for param in $@
	do
		if [[ "$param" =~ ^[0-9]+$ ]]; then
			sedCommand="$sedCommand${param}d;"
		fi
	done
	if [ "$sedCommand" != "" ]; then
		sed -i $sedCommand $todoFilePath;
	fi

else
	read -p "Do you want to delete ALL entries? (y/n):" response;
	if [ $response = "y" ]; then
		shred -u  $todoFilePath;
	fi
fi
}

if [ ! -e $todoFilePath ]; then
	touch "$todoFilePath";
fi

case "$1" in
	"-s" | "--show")
		show;
	;;
	"-a" | "--add")
		shift;
		add "$@";
	;;
	"-d" | "--delete")
		shift;
		delete $@;
	;;
	"-h" | "--help")
		help;
	;;
esac
